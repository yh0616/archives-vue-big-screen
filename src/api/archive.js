import request from '@/utils/request'

// 获取当天提交的数字档案
export function getTodaySubmit(data) {
  return request({
    url: '/chart/getTodaySubmit',
    method: 'post',
    data
  })
}
// 获取月度档案数据
export function getPassRate(data) {
  return request({
    url: '/chart/getPassRate',
    method: 'post',
    data
  })
}