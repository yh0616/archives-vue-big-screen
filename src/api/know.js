import request from '@/utils/knowRequest'

// 知识分享排行榜
export function getRanks(data) {
  return request({
    url: '/drive/getRanks',
    method: 'get',
    params: data
  })
}
// 今日知识通过率
export function getKnowMainPass(data) {
  return request({
    url: '/drive/KnowMainPass',
    method: 'get',
    params: data
  })
}

// 每日知识数据统计图
export function getKnowMainCreate(data) {
  return request({
    url: '/drive/KnowMainCreate',
    method: 'get',
    params: data
  })
}

// 今年知识统计数据
export function getMiddleConclusion(data) {
  return request({
    url: '/drive/getMiddleConclusion',
    method: 'get',
    params: data
  })
}
