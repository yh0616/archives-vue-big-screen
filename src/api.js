import axios from "axios";
// import { Message } from "element-ui";
const baseUrl = "/api";
const url = {
    getTodaySubmit: baseUrl + '/chart/getTodaySubmit',//获取当天提交的数字档案
    getPassRate: baseUrl + "/chart/getPassRate",//获取月度档案数据
};
const duration = {//存储时间表，与url里的键名一致
    tradeTags: 60 * 60 * 1000
}

const get = function (url, params) {
    return new Promise((resolve, reject) => {
        axios
            .get(url, {
                params: params,
            })
            .then((res) => {
                if (res.status == "200") {
                    resolve(res.data);
                } else {
                    reject(res);
                }
            });
    });
};

const getInBody = function (url, params) {
    return new Promise((resolve, reject) => {
        axios
            .get(url, {
                body: params,
            })
            .then((res) => {
                if (res.status == "200") {
                    resolve(res);
                } else {
                    reject(res);
                }
            });
    });
};

const post = function (url, params) {
    return new Promise((resolve, reject) => {
        axios.post(url, params).then((res) => {
            if (res.status == "200") {
                resolve(res.data);
            } else {
                reject(res);
            }
        }).catch((err) => {
            reject(err);
        })
    });
};

const put = function (url, params) {
    return new Promise((resolve, reject) => {
        axios.put(url, params).then((res) => {
            if (res.status == "200") {
                resolve(res);
            } else {
                reject(res);
            }
        });
    });
};

const deleted = function (url, params) {
    return new Promise((resolve, reject) => {
        axios.delete(url, params).then((res) => {
            if (res.status == "200") {
                resolve(res);
            } else {
                reject(res);
            }
        });
    });
};
// 添加请求拦截器，在请求头中加token
axios.interceptors.request.use(
    (config) => {
        if (sessionStorage.getItem("token")) {
            config.headers.Authorization = sessionStorage.getItem("token");
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use(
    (response) => {
        // 如果返回的状态码为200，说明接口请求成功，可以正常拿到数据
        // 否则的话抛出错误
        switch (response.status) {
            case 200:
                return Promise.resolve(response);
            // 401: 未登录 未授权
            // 未登录则跳转登录页面，并携带当前页面的路径
            // 在登录成功后返回当前页面，这一步需要在登录页操作。
            case 401:
                return Promise.reject(response);
            // 403 没有权限
            case 403:
                // Message.error("身份过期，请重新登录！");
                //window.location.href = "/";
                break;

            // 404 请求不存在
            case 404:
                // Message.error(response.data);
                return Promise.reject(response);

            // 其他错误，直接抛出错误提示
            case 500:
                // Message.error(response.data);
                break;
            default:
            // Message.error(response.data);
        }
        return Promise.reject(response.data);
    },

    // 服务器状态码不是2开头的的情况
    // 这里可以跟你们的后台开发人员协商好统一的错误状态码
    // 然后根据返回的状态码进行一些操作，例如登录过期提示，错误提示等等
    // 下面列举几个常见的操作，其他需求可自行扩展
    (error) => {
        switch (error.response.status) {
            // 401: 未登录 未授权
            // 未登录则跳转登录页面，并携带当前页面的路径
            // 在登录成功后返回当前页面，这一步需要在登录页操作。
            case 401:
                return Promise.reject(error.response.status);
            // 403 没有权限
            case 403:
                // Message.error(error.response.data);
                window.location.href = "/";
                break;

            // 404 请求不存在
            case 404:
                // Message.error(error.response.data);
                break;

            // 其他错误，直接抛出错误提示
            case 500:
                // Message.error("服务器内部错误");
                break;

            default:
            // Message.error(error.response.data);
        }
        return Promise.reject(error.response.data);
    }
);

export default {
    url,
    duration,
    get,
    post,
    put,
    getInBody,
    deleted,
};
