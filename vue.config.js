const path = require('path')
const resolve = dir => {
  return path.join(__dirname, dir)
}
module.exports = {
  publicPath: './',
  chainWebpack: config => {
    config.resolve.alias
      .set('_c', resolve('src/components')) // key,value自行定义，比如.set('@@', resolve('src/components'))
  },
  devServer: {
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        target: process.env.VUE_APP_BASE_URL,
        changeOrigin: true,
        ws: true,
        secure: false,
        pathRewrite: {
          ["^" + process.env.VUE_APP_BASE_API]: ""
        }
      },
      [process.env.VUE_APP_KNOW_API]: {
        target: process.env.VUE_APP_KNOW_URL,
        changeOrigin: true,
        ws: true,
        secure: false,
        pathRewrite: {
          ["^" + process.env.VUE_APP_KNOW_API]: ""
        }
      },
      // "/api": {
      //   target: 'http://124.223.214.155:81/prod-api',
      //   // scp -r /Users/cocotte/Vue/newProject/archives-vue-big-screen/dist/* root@124.223.214.155:/usr/local/nginx/bigScreen
      //   // 允许跨域
      //   changeOrigin: true,
      //   pathRewrite: {
      //     '^/api': '/',
      //   }
      // },

    },
  }
}